package com.example.backtitamedia.repository;

import com.example.backtitamedia.entity.Library;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ILibraryDAO extends JpaRepository<Library, Long> {
}
