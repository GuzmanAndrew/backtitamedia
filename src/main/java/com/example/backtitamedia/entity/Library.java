package com.example.backtitamedia.entity;

import javax.persistence.*;

@Entity
@Table(name="libraries")
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "autor")
    private String autor;

    @Column(name = "category")
    private String category;

    @Column(name = "disponibility")
    private int disponibility;

    @Column(name = "price")
    private double price;

    public Library() {
    }

    public Library(String name, String autor, String category, int disponibility, double price) {
        this.name = name;
        this.autor = autor;
        this.category = category;
        this.disponibility = disponibility;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getDisponibility() {
        return disponibility;
    }

    public void setDisponibility(int disponibility) {
        this.disponibility = disponibility;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
