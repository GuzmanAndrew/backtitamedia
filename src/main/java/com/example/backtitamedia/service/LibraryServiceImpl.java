package com.example.backtitamedia.service;

import com.example.backtitamedia.entity.Library;
import com.example.backtitamedia.repository.ILibraryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibraryServiceImpl implements ILibraryService{
    @Autowired
    ILibraryDAO iLibraryDAO;

    @Override
    public List<Library> listBooks() {
        return iLibraryDAO.findAll();
    }

    @Override
    public Library saveBook(Library book) {
        return iLibraryDAO.save(book);
    }

    @Override
    public Library bookId(Long id) {
        return iLibraryDAO.findById(id).get();
    }

    @Override
    public Library updateBook(Library book) {
        return iLibraryDAO.save(book);
    }

    @Override
    public void deleteBook(Long id) {
        iLibraryDAO.deleteById(id);
    }
}
