package com.example.backtitamedia.service;


import com.example.backtitamedia.entity.Library;

import java.util.List;

public interface ILibraryService {
    public List<Library> listBooks();

    public Library saveBook(Library book);

    public Library bookId(Long id);

    public Library updateBook(Library book);

    public void deleteBook(Long id);
}
