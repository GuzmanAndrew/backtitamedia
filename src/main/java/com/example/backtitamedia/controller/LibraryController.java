package com.example.backtitamedia.controller;

import com.example.backtitamedia.entity.Library;
import com.example.backtitamedia.service.LibraryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class LibraryController {

    @Autowired
    LibraryServiceImpl libraryService;

    @GetMapping("/list")
    public List<Library> listarClientes(){
        return libraryService.listBooks();
    }

    @PostMapping("/save")
    public Library guardarCliente(@RequestBody Library book) {
        return libraryService.saveBook(book);
    }

    @GetMapping("/book/{id}")
    public Library clienteId(@PathVariable(name = "id") Long id) {
        Library clienteId = new Library();
        clienteId = libraryService.bookId(id);
        return clienteId;
    }

    @PutMapping("/book/update/{id}")
    public Library actualizarCliente(@PathVariable(name = "id") Long id, @RequestBody Library book) {
        Library bookSelect = new Library();
        Library bookUpdate = new Library();

        bookSelect = libraryService.bookId(id);
        bookSelect.setName(book.getName());
        bookSelect.setAutor(book.getAutor());
        bookSelect.setCategory(book.getCategory());
        bookSelect.setDisponibility(book.getDisponibility());
        bookSelect.setPrice(book.getPrice());

        bookUpdate = libraryService.saveBook(bookSelect);

        return bookUpdate;
    }

    @DeleteMapping("/book/delete/{id}")
    public void clienteDelete(@PathVariable(name = "id") Long id) {
        libraryService.deleteBook(id);
    }
}
