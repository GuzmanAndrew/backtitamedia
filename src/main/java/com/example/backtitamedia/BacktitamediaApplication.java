package com.example.backtitamedia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BacktitamediaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BacktitamediaApplication.class, args);
	}

}
